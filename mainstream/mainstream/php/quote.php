<?php




/*** Request a Quote */

$email_to="support@mainstreamnigeria.com";
$email_from = "caladmin@mainstreamnigeria.com"; // must be different than $email_from 
$email_subject = "Amet table water Quotation Request";

if(isset($_POST['email']))
{

    function return_error($error)
    {
        echo json_encode(array('success'=>0, 'message'=>$error));
        die();
    }

    // check for empty required fields
    if (!isset($_POST['fullname']) ||
        !isset($_POST['email']) ||
		!isset($_POST['product']) ||
        !isset($_POST['qty']) ||
        !isset($_POST['other']))
		
    {
        return_error('Please fill in all required fields.');
    }

    // form field values
    $fullname = $_POST['fullname']; // required
    $email = $_POST['email']; // required
	$product = $_POST['product']; //required
    $qty = $_POST['qty']; // required
	$other = $_POST['other']; // required
	

    // form validation
    $error_message = "";

    // name
    $name_exp = "/^[a-z0-9 .\-]+$/i";
    if (!preg_match($name_exp,$name))
    {
        $this_error = 'Please enter a valid full name.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    }        

    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    if (!preg_match($email_exp,$email))
    {
        $this_error = 'Please enter a valid email address.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    } 

    // if there are validation errors
    if(strlen($error_message) > 0)
    {
        return_error($error_message);
    }

    // prepare email message
    $email_message = "New Quotation Request\n\n";

    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }


    $email_message .= "FullName: ".clean_string($fullname)."\n";
    $email_message .= "Email: ".clean_string($email)."\n";
    $email_message .= "Product: ".clean_string($product)."\n";
    $email_message .= "Quantity: ".clean_string($qty)."\n";
    $email_message .= "Other: ".clean_string($other)."\n";

    // create email headers
    $headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    if (@mail($email_to, $email_subject, $email_message, $headers))
    {
        echo json_encode(array('success'=>1, 'message'=>'Form submitted successfully.')); 
    }

    else 
    {
        echo json_encode(array('success'=>0, 'message'=>'An error occured. Please try again later.')); 
        die();        
    }
}
else
{
    echo 'Please fill in all required fields.';
    die();
}







?>