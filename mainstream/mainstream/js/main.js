
/* Navigation Menu */

function handleClick(){
	document.getElementById("nested-ul").hidden = false;	
}

function handleMouseUp(){
	document.getElementById("nested-ul").hidden = true;	
}

document.getElementById("sub_companies").addEventListener("mouseover", handleClick);
document.getElementById("sub_companies").addEventListener("mouseout", handleMouseUp);


/*** Ajax Form Submissions ****/

//Contact Form
$(function () {
        $('#contact-button').bind('click', function (event) {
			
        // prevent default form submission
        event.preventDefault();

          $.ajax({
            type: 'POST',
            url: 'php/mail.php',
            data: $('#contactForm').serialize(),
            success: function () {
			  alert('message successfully sent');
            }
          });
        });
      });
	  

//Request a quote Form
$(function () {
        $('#request-quote').bind('click', function (event) {
			
        // prevent default form submission
        event.preventDefault();

          $.ajax({
            type: 'POST',
            url: 'php/quote.php',
            data: $('#request-a-quote-form').serialize(),
            success: function () {
			  alert('message successfully sent');
			 //$('#get-a-quote').fadeOut(1000);
			 $('#get-a-quote').modal('hide');
            }
          });
        });
      });
	  
	  
//Order Now
$(function () {
        $('#order-now-button').bind('click', function (event) {
			
        // prevent default form submission
        event.preventDefault();

          $.ajax({
            type: 'POST',
            url: 'php/order.php',
            data: $('#order-now-form').serialize(),
            success: function () {
			 alert('message successfully sent');
			 $('#order-now').modal('hide');
			 
            }
          });
        });
      });
	  